let router = require('express').Router();
let fs = require('fs')
router.get('/', (req, res) => {
    res.json({ 
        "message": "crafted by Hoang Android with love"
    })
})
router.get('/service/hotel/:place_id', (req, res) => {
    fs.readFile('hotel_fake.json', (err, data) => {
        if (err == null) {
            res.send(data)
        } else {
            res.json({
                "error":"point out what file do you want to serve?"
            })
        }
    })
})
module.exports = router;