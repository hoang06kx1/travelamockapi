var express = require('express');
var apiRoute = require('./api-route');
let fs = require('fs')
var app = express();
app.get("/url", (req, res, next) => {
    res.json(["Tony","Lisa","Michael","Ginger","Food"]);
   });
app.use('/api', apiRoute)
app.use((req,res,next) => {
    res.status(404);
    res.json({"Cops": "what're you looking for????"});
})
app.listen(3000, () => {
    console.log("Running express server")
})